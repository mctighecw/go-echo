FROM golang:1.13

WORKDIR /usr/src

RUN go get -u github.com/labstack/echo/...
RUN go get -u go.mongodb.org/mongo-driver/bson
RUN go get -u go.mongodb.org/mongo-driver/mongo

COPY . .

EXPOSE 1323

CMD ["go", "run", "./src/main.go"]
