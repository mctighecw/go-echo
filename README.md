# README

My first project using [Go](https://golang.org/) (_Golang_). It is a basic web server using the [Echo](https://echo.labstack.com/) framework, which is connected to MongoDB.

There is a basic REST API to add, get, update, search (filter) and delete people in the _directory_ database. I have also added JWT authentication for all of the `/api` routes.

## App Information

App Name: go-echo

Created: November 2019; updated January 2020

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/go-echo)

## Tech Stack

- Go
- Echo
- JWT Authentication
- Go Mongo Driver
- MongoDB
- Docker

## To Run

### Setup & Run via Terminal

1. Install Go locally
2. Set `$GOPATH`
3. Install dependencies:

```
$ cd go-echo
$ go get -u github.com/labstack/echo/...
$ go get -u go.mongodb.org/mongo-driver/mongo
$ go get -u go.mongodb.org/mongo-driver/bson
```

3. Initialize database

```
$ go run ./src/scripts/init_db.go
```

4. Drop database, if necessary (later on)

```
$ go run ./src/scripts/drop_db.go
```

5. Start server

```
$ source run_dev.sh
```

Server running at `http://localhost:1323/`

### With Docker

```
$ docker-compose up -d --build
 (set up admin user)
$ source init_docker_db.sh
```

## Routes

See `api_info.md`

Last updated: 2025-01-17
