# API Info

- User login:

```
POST
DATA: { "username": "jsmith", "password": "pw" }
URL: http://localhost:1323/login
```

**All routes below require a valid _JWT_ in the request headers**

- Get user's personal info:

```
GET
HEADERS: { "Authorization": "Bearer eyJhbGc..." }
URL: http://localhost:1323/api/user
```

- Get all users' info (_admins only_):

```
GET
HEADERS: { "Authorization": "Bearer eyJhbGc..." }
URL: http://localhost:1323/api/users
```

- Get all items:

```
GET
HEADERS: { "Authorization": "Bearer eyJhbGc..." }
URL: http://localhost:1323/api/directory/all
```

- Get one item:

```
GET
HEADERS: { "Authorization": "Bearer eyJhbGc..." }
URL: http://localhost:1323/api/directory/one/5dd7fe2e9490160b79ced965
```

_OR_

```
POST
HEADERS: { "Authorization": "Bearer eyJhbGc..." }
DATA: { "id": "5dd7fe2e9490160b79ced965" }
URL: http://localhost:1323/api/directory/one
```

- Search (filter) one or more:

```
POST
HEADERS: { "Authorization": "Bearer eyJhbGc..." }
DATA: { "field": "country", "value": "US" }
URL: http://localhost:1323/api/directory/search
```

- Add new item:

```
POST
HEADERS: { "Authorization": "Bearer eyJhbGc..." }
DATA: {
  "first_name": "Bill",
  "last_name": "Samson",
  "address": "8 Apple Lane",
  "city": "Carytown",
  "state": "PA",
  "postal_code": "45678",
  "country": "USA",
  "tel_nr": "(987)654-3210",
  "email": "bill@samson.com",
  "confirmed": false,
  "rating": 2
}
URL: http://localhost:1323/api/directory/add
```

- Update existing item:

```
POST
HEADERS: { "Authorization": "Bearer eyJhbGc..." }
DATA: { "id": "5dd7fe2e9490160b79ced965", "field": "first_name", "value": "Jon" }
URL: http://localhost:1323/api/directory/update
```

- Delete item:

```
POST
HEADERS: { "Authorization": "Bearer eyJhbGc..." }
DATA: { "id": "5dd7fe2e9490160b79ced965" }
URL: http://localhost:1323/api/directory/delete
```
