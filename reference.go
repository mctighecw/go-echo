// Golang Reference

// Iterate over array
for index, el := range arr {
  fmt.Println(index)
  fmt.Println(el)
}

// Get type (useful when returning something from a function)
fmt.Println(reflect.TypeOf(client))
