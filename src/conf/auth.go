package conf

import (
  "os"

  "github.com/dgrijalva/jwt-go"
  "github.com/labstack/echo/middleware"
)

var (
  JWT_CONFIG middleware.JWTConfig
  JWT_SECRET string
)

type JwtCustomClaims struct {
  Username string `json:"username"`
  Name string `json:"name"`
  Admin bool `json:"admin"`
  jwt.StandardClaims
}

func init() {
  if APP_ENV == "production" {
    JWT_SECRET = os.Getenv("JWT_SECRET")
  } else {
    JWT_SECRET = "secret!"
  }

  JWT_CONFIG = middleware.JWTConfig{
    Claims: &JwtCustomClaims{},
    SigningKey: []byte(JWT_SECRET),
  }
}
