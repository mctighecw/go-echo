package conf

import (
  "os"
)

type (
  App struct {
    Name string
    Port uint
  }

  Db struct {
    Host string
    Port uint
    User string
    Pw string
    AdminDb string
    DbName string
    CollUsers string
    CollName string
  }
)

var (
  APP_CONFIG *App
  DB_CONFIG *Db

  APP_ENV string
  DB_HOST string
  DB_USER string
  DB_PW string
  DB_ADMIN string
  DB_NAME string
  COLL_USERS string
  COLL_NAME string
)

func init() {
  APP_CONFIG = &App {
    Name: "Go Echo",
    Port: 1323,
  }

  APP_ENV = os.Getenv("APP_ENV")
  COLL_USERS = "users"

  if APP_ENV == "production" {
    DB_HOST = os.Getenv("DB_HOST")
    DB_USER = os.Getenv("DB_USER")
    DB_PW = os.Getenv("DB_PW")
    DB_ADMIN = os.Getenv("DB_ADMIN")
    DB_NAME = os.Getenv("DB_NAME")
    COLL_NAME = os.Getenv("COLL_NAME")
  } else {
    DB_HOST = "localhost"
    DB_USER = "admin"
    DB_PW = "mypassword"
    DB_ADMIN = "admin"
    DB_NAME = "directory"
    COLL_NAME = "people"
  }

  DB_CONFIG = &Db {
    Host: DB_HOST,
    Port: 27017,
    User: DB_USER,
    Pw: DB_PW,
    AdminDb: DB_ADMIN,
    DbName: DB_NAME,
    CollUsers: COLL_USERS,
    CollName: COLL_NAME,
  }
}
