package db

import (
  "../lib"
  "../models"

  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  U1 = models.User{
    ID:         primitive.NewObjectID(),
    Username:  "jsmith",
    Name:      "John Smith",
    Admin:     true,
    Password:  lib.HashPassword("john1"),
    CreatedAt: time.Now(),
  }

  U2 = models.User{
    ID:         primitive.NewObjectID(),
    Username:  "swilliams",
    Name:      "Susan Williams",
    Admin:     false,
    Password:  lib.HashPassword("susan1"),
    CreatedAt: time.Now(),
  }

  P1 = models.Person{
    ID:           primitive.NewObjectID(),
    FirstName:    "John",
    LastName:     "Smith",
    Address:      "1 Main Street",
    City:         "Springfield",
    State:        "MA",
    PostalCode:   "23456",
    Country:      "USA",
    TelephoneNr:  "(123)456-7890",
    Email:        "john@smith.com",
    Confirmed:    true,
    Rating:       1,
    CreatedAt:    time.Now(),
    UpdatedAt:    time.Now(),
  }

  P2 = models.Person{
    ID:           primitive.NewObjectID(),
    FirstName:    "Susan",
    LastName:     "Williams",
    Address:      "5 Oak Way",
    City:         "Newtown",
    State:        "CT",
    PostalCode:   "87654",
    Country:      "USA",
    TelephoneNr:  "(456)789-1234",
    Email:        "susan@williams.com",
    Confirmed:    false,
    Rating:       1,
    CreatedAt:    time.Now(),
    UpdatedAt:    time.Now(),
  }

  P3 = models.Person{
    ID:           primitive.NewObjectID(),
    FirstName:    "Sam",
    LastName:     "Jones",
    Address:      "12 Cherry Lane",
    City:         "Harristown",
    State:        "RI",
    PostalCode:   "76543",
    Country:      "USA",
    TelephoneNr:  "(678)901-2345",
    Email:        "sam@jones.com",
    Confirmed:    false,
    Rating:       2,
    CreatedAt:    time.Now(),
    UpdatedAt:    time.Now(),
  }
)
