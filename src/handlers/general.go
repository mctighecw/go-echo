package handlers

import (
  "../conf"

  "fmt"
  "net/http"

  "github.com/labstack/echo"
)

func RedirectToWelcome(c echo.Context) error {
  return c.Redirect(http.StatusTemporaryRedirect, "/welcome")
}

func ReturnWelcome(c echo.Context) error {
  n := conf.APP_CONFIG.Name
  m := fmt.Sprintf("Welcome to %s!", n)
  return c.String(http.StatusOK, m)
}
