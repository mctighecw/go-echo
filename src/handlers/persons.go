package handlers

import (
  "../conf"
  "../db"
  "../lib"
  "../models"

  "fmt"
  "net/http"
  "time"

  "github.com/labstack/echo"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  DbName = conf.DB_CONFIG.DbName
  CollName = conf.DB_CONFIG.CollName
)

func GetAll(c echo.Context) error {
  // GET - returns all people
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)

  p := models.Person{}
  res := []models.Person{}
  cursor, err := coll.Find(ctx, bson.M{})
  lib.LogError(err)

  for cursor.Next(ctx) {
    cursor.Decode(&p)
    res = append(res, p)
  }

  if err != nil {
    e := lib.ReturnError("Error getting people")
    return c.JSON(400, e)
  } else {
    d := lib.ReturnAllPersonsData(res)
    return c.JSON(http.StatusOK, d)
  }
}

func GetOneGet(c echo.Context) error {
  // GET - returns one person by id
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)

  id := c.Param("id")
  objID, err := primitive.ObjectIDFromHex(id)
  res := coll.FindOne(ctx, bson.M{"_id": objID})
  err = res.Err()

  p := models.Person{}
  res.Decode(&p)

  if err != nil {
    e := lib.ReturnError("Person not found")
    return c.JSON(404, e)
  } else {
    d := lib.ReturnPersonData(p)
    return c.JSON(http.StatusOK, d)
  }
}

func GetOnePost(c echo.Context) error {
  // POST - returns one person by id
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)
  m := lib.GetEchoMap(c)

  id := lib.ReturnStringValue(m, "id")
  objID, err := primitive.ObjectIDFromHex(id)
  res := coll.FindOne(ctx, bson.M{"_id": objID})
  err = res.Err()

  p := models.Person{}
  res.Decode(&p)

  if err != nil {
    e := lib.ReturnError("Person not found")
    return c.JSON(404, e)
  } else {
    d := lib.ReturnPersonData(p)
    return c.JSON(http.StatusOK, d)
  }
}

func SearchByTerm(c echo.Context) error {
  // POST - returns filtered people by field and search value
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)
  m := lib.GetEchoMap(c)

  field := lib.ReturnStringValue(m, "field")
  value := lib.ReturnStringValue(m, "value")

  p := models.Person{}
  res := []models.Person{}

  filter := bson.D{{field, primitive.Regex{Pattern: value, Options: ""}}}
  cursor, err := coll.Find(ctx, filter)
  lib.LogError(err)

  for cursor.Next(ctx) {
    cursor.Decode(&p)
    res = append(res, p)
  }

  if err != nil {
    e := lib.ReturnError("Error searching people")
    return c.JSON(400, e)
  } else {
    d := lib.ReturnAllPersonsData(res)
    return c.JSON(http.StatusOK, d)
  }
}

func AddOne(c echo.Context) error {
  // POST - adds one new person
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)
  m := lib.GetEchoMap(c)

  n := models.Person{
    ID: primitive.NewObjectID(),
    FirstName: lib.ReturnStringValue(m, "first_name"),
    LastName: lib.ReturnStringValue(m, "last_name"),
    Address: lib.ReturnStringValue(m, "address"),
    City: lib.ReturnStringValue(m, "city"),
    State: lib.ReturnStringValue(m, "state"),
    PostalCode: lib.ReturnStringValue(m, "postal_code"),
    Country: lib.ReturnStringValue(m, "country"),
    TelephoneNr: lib.ReturnStringValue(m, "tel_nr"),
    Email: lib.ReturnStringValue(m, "email"),
    Confirmed: lib.ReturnBoolValue(m, "confirmed"),
    Rating: lib.ReturnIntValue(m, "rating"),
    CreatedAt: time.Now(),
    UpdatedAt: time.Now(),
  }

  newRes, err := coll.InsertOne(ctx, n)
  lib.LogError(err)

  objectID := newRes.InsertedID.(primitive.ObjectID)
  fmt.Println(objectID)

  if err != nil {
    e := lib.ReturnError("Error adding person")
    return c.JSON(400, e)
  } else {
    r := lib.ReturnOK("Person has been added")
    return c.JSON(http.StatusOK, r)
  }
}

func UpdateOne(c echo.Context) error {
  // POST - updates one person by id
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)
  m := lib.GetEchoMap(c)

  id := lib.ReturnStringValue(m, "id")
  field := lib.ReturnStringValue(m, "field")
  value := lib.ReturnStringValue(m, "value")

  objID, _ := primitive.ObjectIDFromHex(id)

  updateRes, err := coll.UpdateOne(
    ctx,
    bson.M{"_id": objID},
    bson.M{
      "$set": bson.M{
        field: value,
        "updated_at": time.Now(),
      },
    },
  )

  modCount := updateRes.ModifiedCount

  if err != nil {
    e := lib.ReturnError("Error updating person")
    return c.JSON(400, e)
  } else if modCount == 0 {
    e := lib.ReturnError("Person not found")
    return c.JSON(404, e)
  } else {
    r := lib.ReturnOK("Person has been updated")
    return c.JSON(http.StatusOK, r)
  }
}

func DeleteOne(c echo.Context) error {
  // POST - deletes one person by id
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollName)
  m := lib.GetEchoMap(c)

  id := lib.ReturnStringValue(m, "id")
  objID, _ := primitive.ObjectIDFromHex(id)

  deleteRes, err := coll.DeleteOne(ctx, bson.M{"_id": objID})
  lib.LogError(err)

  delCount := deleteRes.DeletedCount

  if err != nil {
    e := lib.ReturnError("Error deleting person")
    return c.JSON(400, e)
  } else if delCount == 0 {
    e := lib.ReturnError("Person not found")
    return c.JSON(404, e)
  } else {
    r := lib.ReturnOK("Person has been deleted")
    return c.JSON(http.StatusOK, r)
  }
}
