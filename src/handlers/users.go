package handlers

import (
  "../conf"
  "../db"
  "../lib"
  "../models"

  "net/http"
  "time"

  "github.com/dgrijalva/jwt-go"
  "github.com/labstack/echo"
  "go.mongodb.org/mongo-driver/bson"
)

var (
  CollUsers = conf.DB_CONFIG.CollUsers
)

func Login(c echo.Context) error {
  // POST - logs a user in with a username and password
  m := lib.GetEchoMap(c)

  username := lib.ReturnStringValue(m, "username")
  password := lib.ReturnStringValue(m, "password")

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollUsers)

  res := coll.FindOne(ctx, bson.M{"username": username})
  err := res.Err()

  user := models.User{}
  res.Decode(&user)
  pwValid := lib.CheckPasswordHash(password, user.Password)

  if err != nil || !pwValid {
    return echo.ErrUnauthorized
  }

  name := user.Name
  admin := user.Admin
  expiration := time.Now().Add(time.Hour * 72)

  claims := &conf.JwtCustomClaims{
    username,
    name,
    admin,
    jwt.StandardClaims{
      ExpiresAt: expiration.Unix(),
    },
  }

  token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

  t, err := token.SignedString([]byte(conf.JWT_SECRET))
  if err != nil {
    return err
  }

  message := username + " has successfully logged in"
  r := lib.ReturnTokenOK(message, t)

  return c.JSON(http.StatusOK, r)
}

func UserInfo(c echo.Context) error {
  // GET - returns the user info from the jwt claims
  user := c.Get("user").(*jwt.Token)
  claims := user.Claims.(*conf.JwtCustomClaims)

  r := map[string]interface{}{
    "status": "User logged in",
    "username": claims.Username,
    "name": claims.Name,
    "admin": claims.Admin,
  }

  return c.JSON(http.StatusOK, r)
}

func UsersInfo(c echo.Context) error {
  // GET - returns information for all users from database
  user := c.Get("user").(*jwt.Token)
  claims := user.Claims.(*conf.JwtCustomClaims)

  // Only admin users are authorized
  if (!claims.Admin) {
    return echo.ErrUnauthorized
  }

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollUsers)

  u := models.User{}
  res := []models.User{}
  cursor, err := coll.Find(ctx, bson.M{})
  lib.LogError(err)

  for cursor.Next(ctx) {
    cursor.Decode(&u)
    res = append(res, u)
  }

  d := lib.ReturnAllUsersData(res)
  return c.JSON(http.StatusOK, d)
}
