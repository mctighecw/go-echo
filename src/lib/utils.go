package lib

import (
  "../models"

  "fmt"
  "os"

  "github.com/labstack/echo"
)

func PrintMessage(m string) {
  fmt.Println(m)
}

func LogError(err error) {
  if err != nil {
    fmt.Println(err)
    return
  }
}

func GetEnv(key, fallback string) string {
  if value, ok := os.LookupEnv(key); ok {
    return value
  }
  return fallback
}

func GetEchoMap(c echo.Context) echo.Map {
  m := echo.Map{}
  if err := c.Bind(&m); err != nil {
    LogError(err)
  }
  return m
}

func ReturnStringValue(m echo.Map, field string) string {
  v := fmt.Sprintf("%v", m[field])
  return v
}

func ReturnBoolValue(m echo.Map, field string) bool {
  i := m[field]
  var j bool = bool(i.(bool))
  return j
}

func ReturnIntValue(m echo.Map, field string) int {
  // Convert a web transmitted int (converted to
  // float64) back to int
  i := m[field]
  var j int = int(i.(float64))
  return j
}

func ReturnTokenOK(m string, t string) interface{} {
  r := map[string]interface{}{"status": "OK", "message": m, "token": t}
  return r
}

func ReturnError(m string) interface{} {
  r := map[string]interface{}{"status": "Error", "message": m}
  return r
}

func ReturnOK(m string) interface{} {
  r := map[string]interface{}{"status": "OK", "message": m}
  return r
}

func ReturnAllPersonsData(d []models.Person) interface{} {
  r := map[string]interface{}{"status": "OK", "data": d}
  return r
}

func ReturnPersonData(d models.Person) interface{} {
  r := map[string]interface{}{"status": "OK", "data": d}
  return r
}

func ReturnAllUsersData(d []models.User) interface{} {
  e := []interface{}{}
  f := map[string]interface{}{}

  for _, el := range d {
    f["id"] = el.ID
    f["username"] = el.Username
    f["name"] = el.Name
    f["admin"] = el.Admin
    f["created_at"] = el.CreatedAt

    e = append(e, f)
  }

  r := map[string]interface{}{"status": "OK", "data": e}
  return r
}
