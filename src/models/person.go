package models

import (
  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

type Person struct {
  ID primitive.ObjectID `bson:"_id" json:"id,omitempty"`
  FirstName string `bson:"first_name" json:"first_name"`
  LastName string `bson:"last_name" json:"last_name"`
  Address string `bson:"address" json:"address"`
  City string `bson:"city" json:"city"`
  State string `bson:"state" json:"state"`
  PostalCode string `bson:"postal_code" json:"postal_code"`
  Country string `bson:"country" json:"country"`
  TelephoneNr string `bson:"tel_nr" json:"tel_nr"`
  Email string `bson:"email" json:"email"`
  Confirmed bool `bson:"confirmed" json:"confirmed"`
  Rating int `bson:"rating" json:"rating"`
  CreatedAt time.Time `bson:"created_at" json:"created_at,omitempty"`
  UpdatedAt time.Time `bson:"updated_at" json:"updated_at,omitempty"`
}
