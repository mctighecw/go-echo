package models

import (
  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
  ID primitive.ObjectID `bson:"_id" json:"id,omitempty"`
  Username string `bson:"username" json:"username"`
  Name string `bson:"name" json:"name"`
  Admin bool `bson:"admin" json:"admin"`
  Password string `bson:"password" json:"password"`
  CreatedAt time.Time `bson:"created_at" json:"created_at,omitempty"`
}
