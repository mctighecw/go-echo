package main

import (
  "../conf"
  "../db"
  "../lib"

  "fmt"

  "go.mongodb.org/mongo-driver/bson"
)

var (
  DbName = conf.DB_CONFIG.DbName
  CollName = conf.DB_CONFIG.CollName
  CollUsers = conf.DB_CONFIG.CollUsers
)

func main() {
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll1 := db.Collection(CollName)
  coll2 := db.Collection(CollUsers)

  f := bson.M{}
  res1, err1 := coll1.DeleteMany(ctx, f)
  res2, err2 := coll2.DeleteMany(ctx, f)

  lib.LogError(err1)
  lib.LogError(err2)
  fmt.Println(res1)
  fmt.Println(res2)
}
