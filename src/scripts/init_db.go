package main

import (
  "../conf"
  "../db"
  "../lib"

  "fmt"
)

var (
  DbName = conf.DB_CONFIG.DbName
  CollUsers = conf.DB_CONFIG.CollUsers
  CollName = conf.DB_CONFIG.CollName

  seeds1 = []interface{}{}
  seeds2 = []interface{}{}
  u1 = db.U1
  u2 = db.U2
  s1 = db.P1
  s2 = db.P2
  s3 = db.P3
)

func main() {
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll1 := db.Collection(CollUsers)
  coll2 := db.Collection(CollName)

  seeds1 = append(seeds1, u1, u2)
  result1, err1 := coll1.InsertMany(ctx, seeds1)

  seeds2 = append(seeds2, s1, s2, s3)
  result2, err2 := coll2.InsertMany(ctx, seeds2)

  lib.LogError(err1)
  lib.LogError(err2)
  fmt.Println(result1)
  fmt.Println(result2)
}
