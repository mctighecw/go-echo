package server

import (
  "../conf"
  "../handlers"
  "../lib"

  "github.com/labstack/echo"
  "github.com/labstack/echo/middleware"
)

func CreateServer() *echo.Echo {
  e := echo.New()

  // Middleware
  e.Use(middleware.Recover())

  // Custom logger format
  e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
    Format: "${time_rfc3339_nano}  ${method}  ${uri}  ${status}  ${error}\n",
  }))

  // CORS
  e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
    AllowOrigins: []string{"*"},
    AllowMethods: []string{echo.HEAD, echo.GET, echo.POST},
  }))

  // Open routes
  e.GET("/", handlers.RedirectToWelcome)
  e.GET("/welcome", handlers.ReturnWelcome)

  // User login
  e.POST("/login", handlers.Login)

  // Authenticated routes
  a := e.Group("/api")
  a.Use(middleware.JWTWithConfig(conf.JWT_CONFIG))
  a.GET("/user", handlers.UserInfo)
  a.GET("/users", handlers.UsersInfo)

  d := a.Group("/directory")
  d.GET("/all", handlers.GetAll)
  d.GET("/one/:id", handlers.GetOneGet)
  d.POST("/one", handlers.GetOnePost)
  d.POST("/search", handlers.SearchByTerm)
  d.POST("/add", handlers.AddOne)
  d.POST("/update", handlers.UpdateOne)
  d.POST("/delete", handlers.DeleteOne)

  // Environment
  m := lib.GetEnv("APP_ENV", "development")
  mode := "Mode: " + m
  lib.PrintMessage(mode)

  return e
}
